﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP3.Extensions
{
    public static class ListExtension
    {
        public static List<T> GetFirstItems<T>(this List<T> list, int count)
        {
            if (list == null)
            {
                throw new ArgumentNullException(nameof(list));
            }

            return list.Take(count).ToList();
        }
    }
}
