﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP3.Models;
using TP3.Services;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using TP3.Extensions;

namespace TP3.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PizzaListPage : ContentPage
    {
        private IPizzaService _pizzaService;

        public PizzaListPage()
        {
            InitializeComponent();

            this._pizzaService = Startup.ServiceProvider.GetService<IPizzaService>();

            this.lstPizzas.ItemsSource = this._pizzaService.Pizzas;

            // List<Pizza> firstPizzas = this._pizzaService.Pizzas.GetFirstItems(2);
        }

        private void lstPizzas_ItemTapped(object sender, ItemTappedEventArgs e)
        {
            Pizza pizza = e.Item as Pizza;

            this.Navigation.PushAsync(new DetailPizzaPage(pizza));
        }
    }
}