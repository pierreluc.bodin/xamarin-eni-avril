﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using TP3.Extensions;

namespace TP3
{
    public static class Startup
    {
        public static IServiceProvider ServiceProvider { get; set; }

        public static IServiceProvider Init()
        {
            ServiceProvider serviceProvider = new ServiceCollection().ConfigureServices().BuildServiceProvider();

            ServiceProvider = serviceProvider;

            return serviceProvider;
        }
    }

}
