﻿using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP3
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Startup.Init();

            // On force la culture FR
            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;

            MainPage = new NavigationPage(new Views.PizzaListPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
