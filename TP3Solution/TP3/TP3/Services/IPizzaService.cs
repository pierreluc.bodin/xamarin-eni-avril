﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3.Models;

namespace TP3.Services
{
    public interface IPizzaService
    {
        List<Pizza> Pizzas { get; }
    }
}
