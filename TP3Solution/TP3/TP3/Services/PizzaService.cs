﻿using System;
using System.Collections.Generic;
using System.Text;
using TP3.Models;

namespace TP3.Services
{
    public class PizzaService : IPizzaService
    {
        private List<Pizza> _Pizzas;

        public List<Pizza> Pizzas
        {
            get
            {
                if (this._Pizzas == null)
                {
                    this._Pizzas = new List<Pizza>
                    {
                        new Pizza { Nom = "Margarita", Ingredients = "Tomates, fromage", Prix = 10.3, EstVegetarien = true },
                        new Pizza { Nom = "Royale", Ingredients = "Jambon, tomates, fromage", Prix = 12 },
                        new Pizza { Nom = "Orientale", Ingredients = "Olives, poivrons, merguez, oignons", Prix = 11.8 }
                    };
                }
                return this._Pizzas;
            }
        }
    }
}
