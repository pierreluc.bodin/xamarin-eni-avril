﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AdditionPage : ContentPage
    {
        public AdditionPage()
        {
            InitializeComponent();
        }

        private void AdditionViewModel_EstMultipleDeTrois(object sender, EventArgs e)
        {
            this.DisplayAlert(
                "Résultat",
                "L'addition des deux nombres donne un multiple de 3.",
                "Fermer");
        }
    }
}