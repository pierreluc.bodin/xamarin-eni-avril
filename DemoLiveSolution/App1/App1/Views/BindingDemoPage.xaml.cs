﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BindingDemoPage : ContentPage
    {
        /// <summary>
        /// On peut binder une propriété public du context
        /// </summary>
        public string HelloBinding { get; set; }
        public BindingDemoPage()
        {
            InitializeComponent();

            this.HelloBinding = "Hello binding";

            // On précise que le contexte de binding est la page en elle même
            this.BindingContext = this;

            this.HelloBinding = "Binding trop tard";
        }
    }
}