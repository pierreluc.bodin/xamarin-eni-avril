﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace App1.ViewModels
{
    public class AdditionViewModel : BaseViewModel
    {
        public event EventHandler EstMultipleDeTrois;

        private int _Nombre1;
        public int Nombre1
        {
            get => this._Nombre1;
            set
            {
                if (this.Set(value, ref this._Nombre1))
                {
                    this.CalculerResultat();
                    this.CmdEffacer.ChangeCanExecute();
                }
            }
        }
        private int _Nombre2;
        public int Nombre2
        {
            get => this._Nombre2;
            set
            {
                if (this.Set(value, ref this._Nombre2))
                {
                    this.CalculerResultat();
                    this.CmdEffacer.ChangeCanExecute();
                }
            }
        }

        private int _Resultat;
        public int Resultat
        {
            get => this._Resultat;
            set
            {
                if (this.Set(value, ref this._Resultat))
                {
                    if (this._Resultat % 3 == 0)
                    {
                        this.EstMultipleDeTrois?.Invoke(this, EventArgs.Empty);

                        //  Application.Current.MainPage = new Views.BindingDemoPage();
                        // Application.Current.MainPage.DisplayAlert()
                    }
                }
            }
        }

        public Command CmdEffacer { get; set; }

        public AdditionViewModel()
        {
            this.CmdEffacer = new Command(this.Effacer, this.CanEffacer);
        }

        private void Effacer()
        {
            this.Nombre1 = 0;
            this.Nombre2 = 0;
        }

        private bool CanEffacer()
        {
            return this.Nombre1 != 0 || this.Nombre2 != 0;
        }

        private void CalculerResultat()
        {
            this.Resultat = this.Nombre1 + this.Nombre2;
        }
    }
}
