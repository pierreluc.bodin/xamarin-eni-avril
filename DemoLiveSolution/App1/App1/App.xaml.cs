﻿using System;
using System.Threading;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App1
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fr-FR");
            Thread.CurrentThread.CurrentCulture = Thread.CurrentThread.CurrentUICulture;

            this.MainPage = new Views.AdditionPage(); // new Views.BindingDemoPage();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
