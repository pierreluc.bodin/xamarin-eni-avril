﻿using Android.App;
using Android.Widget;
using TP4.Droid.Implementations;
using TP4.Services;

[assembly: Xamarin.Forms.Dependency(typeof(ShowNotificationAndroid))]
namespace TP4.Droid.Implementations
{
    public class ShowNotificationAndroid : IShowNotification
    {
        public void ShowNotification(string message)
        {
            Toast.MakeText(Application.Context, message, ToastLength.Long)
                 .Show();
        }
    }
}