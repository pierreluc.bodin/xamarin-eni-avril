﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP4.Services;
using TP4.UWP.Implementations;
using Windows.UI.Popups;

[assembly:Xamarin.Forms.Dependency(typeof(ShowNotificationWindows))]
namespace TP4.UWP.Implementations
{
    public class ShowNotificationWindows : IShowNotification
    {
        public async void ShowNotification(string message)
        {
            MessageDialog dialog = new MessageDialog(message);
            await dialog.ShowAsync();
        }
    }
}
