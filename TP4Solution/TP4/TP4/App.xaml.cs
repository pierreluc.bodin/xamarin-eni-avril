﻿using System;
using TP4.Services;
using TP4.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP4
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(new AuthenticationPage());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
            Application.Current.Properties["OnSleep"] = DateTime.Now;                        
        }

        protected override void OnResume()
        {
            if (DateTime.TryParse(Application.Current.Properties["OnSleep"]?.ToString(), out DateTime onSleep))
            {
                TimeSpan duration = DateTime.Now - onSleep;

                if (duration.TotalSeconds > 10)
                {
                    UserBankService.Instance.Logout();
                }
            }
        }
    }
}
