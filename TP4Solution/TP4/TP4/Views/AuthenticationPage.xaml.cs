﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TP4.Services;
using TP4.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP4.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AuthenticationPage : ContentPage
    {
        public AuthenticationPage()
        {
            InitializeComponent();
        }

        private void AuthenticationViewModel_OnLoginResult(object sender, bool success)
        {
            if (success)
            {
                this.Navigation.PushAsync(new UserBankDetailPage());
            }else
            {
                //this.DisplayAlert(
                //    "Authentification", 
                //    "Échec de la connexion ; Identifiant(s) invalide(s).", "Fermer");

                DependencyService.Get<IShowNotification>().ShowNotification("Identifiant(s) invalide(s)");
            }
        }
    }
}