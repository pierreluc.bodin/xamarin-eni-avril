﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP4.Models
{
    public class UserBank
    {
        public string Email { get; set; }
        public int IdCompte { get; set; }

        public string Nom { get; set; }

        public string Prenom { get; set; }

        public string Mdp { get; set; }
    }
}
