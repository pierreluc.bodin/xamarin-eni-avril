﻿using System;
using System.Collections.Generic;
using System.Text;
using TP4.Models;
using TP4.Services;
using Xamarin.Forms;

namespace TP4.ViewModels
{
    public class UserBankViewModel : BaseViewModel
    {
        public string NomPrenom { get; set; }

        public Command CmdLogout { get; set; }

        public UserBankViewModel()
        {
            UserBank user = UserBankService.Instance.AuthenticatedUser;
            
            if (user != null)
            {
                this.NomPrenom = $"{user.Nom} {user.Prenom}";
            }

            this.CmdLogout = new Command(this.Logout);
        }

        private void Logout()
        {
            UserBankService.Instance.Logout();
        }
    }
}
