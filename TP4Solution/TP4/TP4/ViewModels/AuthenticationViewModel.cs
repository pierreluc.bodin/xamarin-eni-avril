﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using TP4.Services;
using Xamarin.Forms;
using TP4.Extensions;

namespace TP4.ViewModels
{
    public class AuthenticationViewModel : BaseViewModel
    {
        public event EventHandler<bool> OnLoginResult;

        private string _IdCompte;
        public string IdCompte
        {
            get => this._IdCompte;
            set
            {
                if (this.Set(value, ref this._IdCompte))
                {
                    this.CmdLogin.ChangeCanExecute();
                }
            }
        }

        private string _Password;
        public string Password
        {
            get => this._Password;
            set
            {
                if (this.Set(value, ref this._Password))
                {
                    this.CmdLogin.ChangeCanExecute();
                    this.CmdErase.ChangeCanExecute();
                }
            }
        }

        public Command CmdLogin { get; set; }

        public Command CmdErase { get; set; }

        public Command<int> CmdAddDigit { get; set; }

        public List<int> Digits { get; set; }

        public AuthenticationViewModel()
        {
            // this.CmdLogin = new Command(this.Login, this.CanLogin);
            this.CmdLogin = new Command(() => this.Login(), () => this.CanLogin());
            this.CmdErase = new Command(this.Erase, this.CanErase);
            this.CmdAddDigit = new Command<int>(i => this.AddDigit(i));

            this.Digits = Enumerable.Range(0, 10).Randomize().ToList();
        }

        private void AddDigit(int i)
        {
            if (this.Password == null)
            {
                this.Password = string.Empty;
            }

            this.Password += i;
        }

        private void Login()
        {
            bool success = UserBankService.Instance.Login(int.Parse(this.IdCompte), this.Password);
            Debug.WriteLine("Login success : " + success);

            this.OnLoginResult?.Invoke(this, success);

            if (success)
            {
                this.Erase();
            }
        }

        private bool CanLogin()
        {
            if (!int.TryParse(this.IdCompte, out int id))
            {
                return false;
            }

            if (this.IdCompte.Length != 10)
            {
                return false;
            }

            if (!this.IdCompte.StartsWith("123"))
            {
                return false;
            }

            if (string.IsNullOrEmpty(this.Password))
            {
                return false;
            }

            if (this.Password.Length != 4)
            {
                return false;
            }

            return true;
        }

        private void Erase()
        {
            this.Password = null;
        }

        private bool CanErase()
        {
            return !string.IsNullOrEmpty(this.Password);
        }
    }
}
