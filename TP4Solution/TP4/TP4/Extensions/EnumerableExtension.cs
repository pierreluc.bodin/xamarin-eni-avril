﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TP4.Extensions
{
    public static class EnumerableExtension
    {
        public static IEnumerable<T> Randomize<T>(this IEnumerable<T> enumerable)
        {
            return enumerable.OrderBy(i => Guid.NewGuid());
        }
    }
}
