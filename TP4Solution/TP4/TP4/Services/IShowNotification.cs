﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TP4.Services
{
   public interface IShowNotification
    {
        void ShowNotification(string message);
    }
}
