﻿using System;
using System.Collections.Generic;
using System.Text;
using TP4.Models;

namespace TP4.Services
{
    public interface IUserBankService
    {
        bool Login(int idCompte, string mdp);

        void Logout();

        UserBank AuthenticatedUser { get; }

        bool IsAuthenticated { get; }
    }
}
