﻿using System;
using System.Collections.Generic;
using System.Text;
using TP4.Models;
using Xamarin.Forms;

namespace TP4.Services
{
    public class UserBankService : IUserBankService
    {
        #region Singleton
        private UserBankService() { }

        private static UserBankService _Instance;

        public static IUserBankService Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new UserBankService();
                }
                return _Instance;
            }
        }

        #endregion

        private UserBank _userBank;

        public UserBank AuthenticatedUser => this._userBank;

        public bool IsAuthenticated => this._userBank != null;

        public bool Login(int idCompte, string mdp)
        {
            if (idCompte != 1234567890 || mdp != "1234")
            {
                return false;
            }

            this._userBank = new UserBank
            {
                IdCompte = idCompte,
                Email = "email@moq",
                Mdp = mdp,
                Nom = "Jean",
                Prenom = "Michel"
            };

            return true;
        }

        public void Logout()
        {
            this._userBank = null;

            Application.Current.MainPage.Navigation.PopToRootAsync();
        }
    }
}
