﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TP2.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ColorPickerPage : ContentPage
    {
        public ColorPickerPage()
        {
            InitializeComponent();

            Random random = new Random();

            // Random initialization
            this.sliderR.Value = random.NextDouble();
            this.sliderG.Value = random.NextDouble();
            this.sliderB.Value = random.NextDouble();
        }

        private void slider_ValueChanged(object sender, ValueChangedEventArgs e)
        {
            this.boxView.Color = Color.FromRgb(this.sliderR.Value, this.sliderG.Value, this.sliderB.Value);
        }
    }
}